<?php
    session_start();

    if(!isset($_SESSION['udanarejestracja'])) {
        header('Location: index.php');
        exit();
    }   
    
    else{
        unset($_SESSION['udanarejestracja']);
    }

    
    //Usuwanie zmiennych pamiętających wartości wpisane do formularza

    if(isset($_SESSION['fr_nick'])) unset($_SESSION['fr_nick']);
    if(isset($_SESSION['fr_email'])) unset($_SESSION['fr_email']);
    if(isset($_SESSION['fr_haslo1'])) unset($_SESSION['fr_haslo1']);
    if(isset($_SESSION['fr_haslo2'])) unset($_SESSION['fr_haslo2']);
    if(isset($_SESSION['fr_regulamin'])) unset($_SESSION['fr_regulamin']);

    //Usuwanie błędów rejestracji

    if(isset($_SESSION['e_nick'])) unset($_SESSION['e_nick']);
    if(isset($_SESSION['e_email'])) unset($_SESSION['e_email']);
    if(isset($_SESSION['e_haslo'])) unset($_SESSION['e_haslo']);
    if(isset($_SESSION['e_regulamin'])) unset($_SESSION['e_regulamin']);


?>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8" />    
<title>Osadnicy - gra przeglądarkowa</title>
<script src='https://www.google.com/recaptcha/api.js'></script>

</head> 
<body>

Dziekujemy za rejestrację w serwisie! Możesz już zalogować się na swoje konto! <br><br>

<a href="index.php">Zaloguj się na swoj konto!</a> <br><br>

<form action="zaloguj.php" method="post">
   Login: <br> 
   <input type="text" name="login" /> <br>

   Hasło: <br>
   <input type="password" name="haslo" /> <br><br>
   
   <input type="submit" value="Zaloguj się" />
</form>

<?php
    if(isset($_SESSION['blad'])){
        echo $_SESSION['blad'];
    }
?>
</body>
<html>    